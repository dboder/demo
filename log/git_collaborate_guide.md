# Git项目合作指北

## 综述
> 描述几种常用的软件开发合作模式下如何利用git进行协作

## 背景
> 从ch2开始蟒营开始以小组方式协作开发，需要尽快熟悉协作方式以及明确协作规范


## 环境
> 解决办法适用的环境/版本：
- 软件：git/gitlab
- 仓库：https://gitlab.com/101camp/5py/tasks/-/tree/队长分支


## 问题
> 在几种不同的团队合作模式下如何利用git进行协同？尤其是在蟒营现阶段（从ch2开始）背景、环境下如何协同？


## 操作
> 在软件开发过程中团队协作根据项目情况分为三种模式：
1. Fork和Pull模式
    - 适用场合：项目成员组织松散、大型项目、开源项目
    - 参与者：
        + 项目拥有者：一般是一个人或一个团队
        + 项目贡献者：可以是多人
    - 协作流程：
        1. 项目贡献者在项目拥有者github/gitlab项目仓库上选择fork，将项目fork到自己的项目仓库中
        2. 项目贡献者将fork的项目clone到本地
        3. 项目贡献者在本地修改代码后，add-commit-push到自己的远端仓库
        4. 项目贡献者提交一个`pull request`给项目所有者
        5. 项目所有者审查修改后`merge request`

2. 共享项目仓库模式
    - 适用场合：项目成员组织紧密、中小型项目、公司或团队项目
    - 参与者：
        + 多个开发者同时参与，需要有一个项目管理者
    - 协作流程：
        1. 把项目仓库clone到本地
        2. 更新你本地的仓库，使其与远端仓库保持同步：
            `git pull origin master`
        3. 建立一个新的分支：
            `git checkout -b newbranch`
        4. 在新的分支里修改、add、commit
        5. 把新分支的变化上传到远端仓库：
            `git push origin newbranch`
        6. 在远端仓库选择新的分支，选择`Pull Request`，填写说明（可以@项目管理者），选择`Send Pull Request`按钮
        7. 项目管理者审查修改后`merge request`
        8. 重复第2到第8步

    - 注意事项：
        + 千万不要直接commit到master分支！
        + 永远在你自己建的分支里编辑修改！

3. 蟒营课程模式
    - 适用场合：蟒营课程阶段
    - 背景说明：因为课程要求使用队长的分支作为小组协力分支，而队长分支也是私人分支，无法采用第二种模式
    - 参与者：
        + 队长
        + 队员
    - 协作流程：
        1. 小组成员首次登录，切换到队长分支：
            `git checkout 队长分支`
        2. 更新本地仓库，使其与远端仓库保持同步：
            `git pull`
        3. 在本地仓库进行修改、add、commit
        4. 把本地仓库的变化上传到远端仓库：
            `git push origin 队长分支`
        5. 如果系统提示冲突：
            ```
            ! [rejected]        master -> master (fetch first)
            error: failed to push some refs to 'https://gitlab.com/101camp/5py/playground.git'
            hint: Updates were rejected because the remote contains work that you do
            hint: not have locally. This is usually caused by another repository pushing
            hint: to the same ref. You may want to first integrate the remote changes
            hint: (e.g., 'git pull ...') before pushing again.
            hint: See the 'Note about fast-forwards' in 'git push --help' for details.
            ```
            首先更新本地仓库，使其与远端仓库保持同步：
            `git pull`
        6. 如果没有代码冲突，系统会出现一个提示输入merge信息的文本编辑器，输入信息后保存退出。git会自动merge你本地和远程仓库的信息
            ```
            Merge branch 'dboder' of https://gitlab.com/101camp/5py/tasks into dboder

            # Please enter a commit message to explain why this merge is necessary,
            # especially if it merges an updated upstream into a topic branch.
            #
            # Lines starting with '#' will be ignored, and an empty message aborts
            # the commit.
            ~
            ~
            ~
            ~
            "~/Coding/Python/101camp/tasks/.git/MERGE_MSG" 7L, 301C
            ```
        7. 如果有代码冲突，git会提示冲突文件，这时候用文本编辑器打开冲突文件，可以看到类似信息：
            ```
            <<<<<<<<<<<<< HEAD
            our code
            ========
            Other persons's code
            >>>>>>>>
            ```
            你需要根据情况修改你或队友的代码，并且保存。
        8. add、commit、push你的修改
        9. 重复第3步到第9步
    - 注意事项：
        + 因为每个人都在一个分支修改，需要提前做好分工和统一代码规范，尽量减少冲突
        + 在本地仓库修改前先执行`git pull`确保本地仓库和远端仓库保持同步


## 总结
> 再次说明经验应用要注意的, 容易出问题的点, 以及有助记忆的作弊条...

**仔细阅读注意事项。**


## refer
> 过程中参考过的重要文章/图书/模块/代码/...

永远的: [提问的智慧](https://github.com/DebugUself/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)

- [Collaborating on GitHub](https://uoftcoders.github.io/studyGroup/lessons/git/collaboration/lesson/)


## logging:
> 用倒序日期排列来从旧到新记要关键变化


- 200316 bruceq619 init
